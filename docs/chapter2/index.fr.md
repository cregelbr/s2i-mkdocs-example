# Chapitre 2

Ceci est le deuxième chapitre en français.

## Section 1

Ceci est la première section du deuxième chapitre

Voici un **texte en gras**. Voici du *texte en italique*.

MkDocs fournit un [bref guide sur la manipulation avec Markdown](https://www.mkdocs.org/user-guide/writing-your-docs/#writing-with-markdown){target=_blank}.

## Section 2

Il s'agit de la deuxième section du deuxième chapitre.

### Sous-section 1

Il s'agit de la première sous-section de la deuxième section du deuxième chapitre.

#### Sous-sous-section 1

Il s'agit de la première sous-sous-section de la première sous-section de la deuxième section du deuxième chapitre.

##### Niveau 5

Il existe encore un autre niveau (5e) de structure de contenu.

###### Niveau 6

Il existe un dernier niveau (6e) de structure de contenu.
